import { api } from './../Utils/enums';
import { authorizeRequest } from '../Utils/requests';

const TransactionService = {
  getScannerTransactions: () => getScannerTransactions(),
  getTransactionByWalletId: (walletId, tokens) => getTransactionByWalletId(walletId, tokens),
  checkTransactionStatusByTransactionId: transactionId => checkTransactionStatusByTransactionId(transactionId),
  rejectTransaction: transactionId => rejectTransaction(transactionId),
  getTransactionDetails: (id) => getTransactionDetails(id),
};
const getScannerTransactions = () => {
  const url = api.SCANNER_TRANSACTIONS;
  return authorizeRequest().then(a => a.get(url));
};

const getTransactionByWalletId = (walletId, tokens) => {
  const url = api.TRANSACTION_BY_WALLET_ID;
  const params = {
    walletId,
    tokens,
  };
  return authorizeRequest().then(a => a.post(url, params));
};

const checkTransactionStatusByTransactionId = transactionId => {
  const url = api.CHECK_TRANSACTION_STATUS + '/' + transactionId;
  return authorizeRequest().then(a => a.get(url));
};

const rejectTransaction = transactionId => {
  const url = api.REJECT_TRANSACTION + '/' + transactionId;
  return authorizeRequest().then(a => a.delete(url));
};

const getTransactionDetails = (id) => {
  const url = api.TRANSACTION_DETAILS + "/" + id;
  return authorizeRequest().then(a => a.get(url));
};


export default TransactionService;
