import { api } from "./../Utils/enums"
import { authorizeRequest } from "../Utils/requests";

const TokenService = {
    getTokenDetails: id => getTokenDetails(id),
    scanWalletId: walletId => scanWalletId(walletId),
    useTokens: (walletId, tokenIds) => useTokens(walletId, tokenIds)
};
const getTokenDetails = id => {
    const url = api.TOKEN_DETAILS;
    const data = { containerId: id };
    return authorizeRequest().then(a => a.post(url, data));
};

const scanWalletId = walletId => {
    const url = api.WALLET_SCAN + "/" + walletId;
    return authorizeRequest().then(a => a.get(url));
};

const useTokens = (walletId, tokenIds) => {
    const url = api.USE_TOKENS;
    const data = {
        walletId: walletId,
        tokens: tokenIds
    };
    return authorizeRequest().then(a => a.post(url, data));
};

export default TokenService;