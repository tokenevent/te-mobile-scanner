import { api } from './../Utils/enums';
import { authorizeRequest } from '../Utils/requests';

const EventService = {
  getAllEvents: () => getAllEvents(),
  getEventById: eventId => getEventById(eventId)
};

const getAllEvents = () => {
  const url = api.ALL_EVENTS;
  return authorizeRequest().then(a => a.get(url));
};

const getEventById = eventId => {
  const url = api.EVENT + "/" + eventId;
  return authorizeRequest().then(a => a.get(url));
};

export default EventService;
