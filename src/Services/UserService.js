import {api} from "./../Utils/enums"
import { authorizeRequest, request } from '../Utils/requests';

const UserService = {
    loginUser: userData => loginUser(userData),
    getMe: () => getMe()
};
const loginUser = userData => {
    const url = api.LOGIN_USER;
    return request().post(url, userData);
};

const getMe = () => {
    const url = api.ME;
    return authorizeRequest().then(a => a.get(url));
};

export default UserService;
