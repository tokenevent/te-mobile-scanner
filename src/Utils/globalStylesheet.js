import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    fontSize: 51,
    color: 'black',
    fontFamily: 'TwCenMT-Condensed',
  },
  yellowButtonContainer: {
    fontWeight: 'bold',
    height: 60,
    marginTop: 5,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#ff9811',
    borderRadius: 5,
    justifyContent: 'center',
    textAlign: 'center',
  },
  yellowButtonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
  },
  normalTextInput: {
    color: '#fff',
    height: 45,
    paddingLeft: 10,
    borderRadius: 5,
    backgroundColor: '#1463c1',
    margin: 15,
  },
  normalBlueButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0E7AF5',
    height: 75,
  },
  normalCancelButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F50E0E',
    height: 75,
  },
  normalBrownButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#54494B',
    height: 50,
  },
  normalLightBlueButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#597081',
    height: 50,
  },
  normalYellowButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff9811',
    height: 50,
  },
  normalBlueButtonText: {
    color: '#fff',
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 24,
  },
  boldText: {
    fontWeight: 'bold',
  },
  viewTitleWrapper: {
    marginLeft: 15,
    marginBottom: 10,
    marginTop: 10,
  },
  viewTitle: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 51,
    color: 'black',
  },
  viewDescription: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 28,
  },
  normalBlueButtonContainerWithoutRadius: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0E7AF5',
    height: 70,
    width: '100%',
  },
  tokenName: {
    textTransform: 'uppercase',
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 31
  },
  slash: {
    fontFamily: 'TwCenMT',
    fontSize: 14,
    fontWeight: '800',
    color: '#C8C8C8',
    marginLeft: 10,
    marginRight: 10
  },
  unitPrice: {
    fontFamily: 'TwCenMT',
    fontSize: 18,
    fontWeight: '800',
    color: 'black'
  },
  ordered: {
    fontFamily: 'TwCenMT-Condensed',
    textTransform: 'uppercase',
    fontSize: 18
  },
  orderedAmount: {
    fontFamily: 'TwCenMT-Condensed',
    color: 'black',
    fontSize: 18,
    marginRight: 3
  },
  szt: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 18
  },
  availableTokens: {
    fontFamily: 'TwCenMT',
    fontSize: 32,
    fontWeight: '800',
    color: 'black'
  },
  availableQty: {
    fontFamily: "TwCenMT-Condensed",
    fontSize: 32,
  },
});
