
export const toPriceConverter = number => {
    return `${(number / 100).toLocaleString('pl-PL', {
                    style: 'currency',
                    currency: 'PLN'
                })}`;
};