
export const isUsernameValid = username => {
    return username.length > 0 && username.indexOf(" ") === -1;
};