import AsyncStorage from '@react-native-community/async-storage';

const logout = async navigation => {
  await AsyncStorage.clear();
  navigation.navigate('Auth');
};
export default logout;
