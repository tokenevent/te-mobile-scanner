const errorCodes = {
  NOT_ENOUGH_TOKENS: {
    code: 409,
    title: "Błąd",
    description : "Brak wymaganej liczby żetonów."
  },
  TRANSACTION_REJECTED: {
    title: "Odrzucono",
    description : "Transakcja została odrzucona."
  }
};
export default errorCodes
