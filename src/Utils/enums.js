const apiUrl = "http://tokeneventapp.herokuapp.com";
export const api = {
    TOKEN_DETAILS: apiUrl + "/tokens/container/scan",
    LOGIN_USER: apiUrl + "/public/users/login",
    WALLET_SCAN: apiUrl + "/tokens",
    USE_TOKENS: apiUrl + "/tokens/use",
    SCANNER_TRANSACTIONS: apiUrl + "/scanner/transactions-history",
    ALL_EVENTS: apiUrl + "/events",
    TRANSACTION_BY_WALLET_ID: apiUrl + "/scanner/transactions",
    CHECK_TRANSACTION_STATUS: apiUrl + "/scanner/transactions",
    ME: apiUrl + "/users/me",
    REJECT_TRANSACTION: apiUrl + "/scanner/transactions",
    EVENT: apiUrl + "/events",
    TRANSACTION_DETAILS: apiUrl + "/transaction-details",
};
