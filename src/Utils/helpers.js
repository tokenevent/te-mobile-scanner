export const filterSelectedOptions = (original, selected) => {
  const selectedTokensParsed = selected.map(x => x.type);
  return original.filter(f => !selectedTokensParsed.includes(f.value))
};

export const toPlnString  = (value) => {
  return (value / 100).toLocaleString('pl-PL', {
    currency: 'PLN'
  });
};
