const transactionStatus = {
  ACCEPTED: 'ACCEPTED',
  REJECTED: 'REJECTED',
};
export default transactionStatus;
