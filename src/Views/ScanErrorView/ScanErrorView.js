import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import close from './../../../assets/images/close.png';
import globalStylesheet from '../../Utils/globalStylesheet';
import styles from './ScanErrorView.style';

const ScanErrorView: () => React$Node = ({ navigation }) => {

  const errorTitle = navigation.getParam('errorTitle', 'Odrzucono');
  const errorDescription = navigation.getParam('errorDescription', 'Transakcja została odrzucona.');


  return (
    <View style={globalStylesheet.container}>
      <View style={styles.contentContainer}>
        <Image
          resizeMode="contain"
          source={close}
        />
        <Text style={styles.cancelHeader}>{errorTitle}</Text>
        <Text style={styles.cancelDesc}>{errorDescription}</Text>
      </View>
      <View>
        <View>
          <TouchableOpacity
            style={globalStylesheet.normalBlueButtonContainer}
            onPress={() => navigation.navigate('Main')}
          >
            <Text style={globalStylesheet.normalBlueButtonText}>WRÓĆ</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default ScanErrorView;


