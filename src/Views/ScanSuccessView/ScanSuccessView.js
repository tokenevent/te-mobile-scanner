import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import success from "./../../../assets/images/success.png";
import globalStylesheet from '../../Utils/globalStylesheet';
import styles from './ScanSuccessView.style';

const ScanSuccessView: () => React$Node = ({ navigation }) => {
  return (
    <View style={globalStylesheet.container}>
      <View style={styles.contentContainer}>
        <Image
          resizeMode="contain"
          source={success}
        />
        <Text style={styles.header}>Sukces!</Text>
        <Text style={styles.desc}>Możesz bezpiecznie wydać produkty</Text>
      </View>
      <View>
        <View>
          <TouchableOpacity
            style={globalStylesheet.normalBlueButtonContainer}
            onPress={() => navigation.navigate('Sample')}
          >
            <Text style={globalStylesheet.normalBlueButtonText}>PRODUKTY ZOSTAŁY WYDANE</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default ScanSuccessView


