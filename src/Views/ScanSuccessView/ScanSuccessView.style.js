import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  contentContainer: {
    padding: 15,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: { fontFamily: 'TwCenMT', color: 'black', fontSize: 75 },
  desc: { fontFamily: 'TwCenMT', color: '#858585', fontSize: 20, textAlign: 'center' }
});

export default styles;
