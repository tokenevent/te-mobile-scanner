import { StyleSheet } from "react-native";
import globalStylesheet from '../../Utils/globalStylesheet';

const styles = StyleSheet.create({
  container: {
    ...globalStylesheet.container,
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#084da0',
  },
  logo: {
    width: undefined,
    height: 80,
    margin: 15
  }
});

export default styles;
