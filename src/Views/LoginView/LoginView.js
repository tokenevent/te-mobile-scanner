import React, { useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { ActivityIndicator, Image, Text, TextInput, ToastAndroid, TouchableOpacity, View } from 'react-native';
import styles from './LoginView.style';
import { isUsernameValid } from '../../Utils/validators';
import UserService from '../../Services/UserService';
import globalStylesheet from '../../Utils/globalStylesheet';
import logoWhiteMedium from './../../../assets/images/logoWhiteMedium.png';
import EventService from '../../Services/EventService';

const LoginView = ({ navigation }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setLoading] = useState(false);

  const onPress = () => {
    if (isUsernameValid(username) && isUsernameValid(password)) {
      setLoading(true);

      const userData = {
        username: username,
        password: password,
      };

      UserService.loginUser(userData)
        .then(response => loginUserSuccessHandler(response))
        .catch(error => loginUserErrorHandler(error));
    } else {
      ToastAndroid.show('Wprowadzone dane są nieprawidłowe.', ToastAndroid.SHORT);
    }
  };

  const getEventSuccessHandler = async response => {
    const eventName = response.data.name;
    await AsyncStorage.setItem('eventName', eventName);
    setLoading(false);
    navigation.navigate('App');
  };

  const getEventErrorHandler = e => {
    console.log('e', e);
    ToastAndroid.show('Wystąpił błąd podczas pobierania danych wydarzenia...', ToastAndroid.SHORT);
  };

  const getMeSuccessHandler = async response => {
    const userName = response.data.user.username;
    EventService.getEventById(response.data.user.eventId)
      .then(r => getEventSuccessHandler(r))
      .catch(e => getEventErrorHandler(e));
    await AsyncStorage.setItem('userName', userName);
  };

  const getMeErrorHandler = err => {
    console.log('err', err);
    setLoading(false);
    ToastAndroid.show('Wystąpił błąd podczas pobierania danych użytkownika...', ToastAndroid.SHORT);
  };

  const loginUserSuccessHandler = async response => {
    await AsyncStorage.setItem('accessToken', response.data);
    setUsername('');
    setPassword('');
    await UserService.getMe()
      .then(response => getMeSuccessHandler(response))
      .catch(err => getMeErrorHandler(err));
  };

  const loginUserErrorHandler = error => {
    setLoading(false);
    setUsername('');
    setPassword('');
    ToastAndroid.show('Wystąpił błąd podczas logowania...', ToastAndroid.SHORT);
  };

  return (
    <View style={styles.container}>
      {!isLoading ?
        <View>
          <Image
            style={styles.logo}
            source={logoWhiteMedium}
            resizeMode="contain"
          />
          <TextInput
            style={globalStylesheet.normalTextInput}
            placeholderTextColor="#77b3fb"
            placeholder="Login..."
            onChangeText={username => setUsername(username)}
          />
          <TextInput
            secureTextEntry={true}
            style={globalStylesheet.normalTextInput}
            placeholderTextColor="#77b3fb"
            placeholder="Hasło..."
            onChangeText={password => setPassword(password)}
          />
          <TouchableOpacity onPress={onPress}>
            <View style={globalStylesheet.yellowButtonContainer}>
              <Text style={globalStylesheet.yellowButtonText}>ZALOGUJ</Text>
            </View>
          </TouchableOpacity>
        </View>
        :
        <View>
          <ActivityIndicator size="large" color="#ff9811"/>
        </View>
      }
    </View>
  );
};

export default LoginView;

