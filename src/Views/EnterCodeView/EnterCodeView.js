import React, { useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import globalStylesheet from '../../Utils/globalStylesheet';
import styles from './EnterCodeView.style';

const EnterCodeView: () => React$Node = ({ navigation }) => {
  const [code, setCode] = useState('');
  const tokensData = navigation.getParam('tokensData', {});

  const validate = React.useCallback(code => {
    if (/^[0-9]{0,6}$/.test(code)) {
      setCode(code);
    }
  }, [setCode]);

  return (
    <View style={globalStylesheet.container}>
      <View style={styles.contentContainer}>
        <Text style={globalStylesheet.header}>Wprowadź kod</Text>
        <View style={styles.textContainer}>
          <TextInput
            onChangeText={amount => validate(amount)}
            placeholder="Kod"
            value={code}
            style={styles.codeInput}
            keyboardType="numeric"
          />
        </View>
      </View>
      <View>
        <View>
          <TouchableOpacity
            onPress={() => navigation.push('WaitingAccept', { tokens: tokensData, walletId: code })}
            style={globalStylesheet.normalBlueButtonContainer}>
            <Text style={globalStylesheet.normalBlueButtonText}>WYŚLIJ</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.cancelBtn}>
          <TouchableOpacity style={globalStylesheet.normalCancelButtonContainer}
                            onPress={() => navigation.navigate('Main')}>
            <Text style={globalStylesheet.normalBlueButtonText}>ANULUJ</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default EnterCodeView;


