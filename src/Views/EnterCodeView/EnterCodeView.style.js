import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  textContainer: {
    marginTop: '20%',
  },
  contentContainer: {
    padding: 15, flex: 1,
  },
  codeInput: {
    fontSize: 28,
    height: 60,
    borderBottomColor: '#707070',
    borderBottomWidth: 2,
    width: '100%',
    color: 'black',
    fontFamily: 'TwCenMT',
  },
  cancelBtn: {
    marginTop: 5
  }

});

export default styles;
