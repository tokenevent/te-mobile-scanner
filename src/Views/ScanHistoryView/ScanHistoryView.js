import React, { useEffect, useState } from 'react';
import { ActivityIndicator, ToastAndroid, View } from 'react-native';
import TransactionService from '../../Services/TransactionService';
import ScanHistoryList from '../../Components/ScanHistoryList/ScanHistoryList';
import styles from './ScanHistoryView.style';
import { NavigationEvents } from 'react-navigation';

const ScanHistoryView: () => React$Node = ({ navigation, ...props }) => {
  const [transactions, setTransactions] = useState([]);
  const [isLoaded, setLoaded] = useState(false);

  const isDataLoaded = () => isLoaded;

  const getScannerTransactions = () => {
    TransactionService.getScannerTransactions()
      .then(response => {
        setTransactions(response.data.transactions);
        setLoaded(true);
      })
      .catch(error => {
        ToastAndroid.show('Wystąpił błąd podczas pobierania transakcji...', ToastAndroid.SHORT);
        setLoaded(true);
      });
  };


  const getRejectedOnly = (transaction) => {
    return transaction.status === "REJECTED";
  }

  const getAcceptedOnly = (transaction) => {
      return transaction.status === "ACCEPTED";
  }


  return (
    <View style={styles.container}>
      <NavigationEvents
        onDidFocus={() => getScannerTransactions()}
      />
      {isDataLoaded() &&
      <View style={styles.transactionListWrapper}>
        <ScanHistoryList
          transactions={transactions.sort((a, b) => new Date(b.transactionAt) - new Date(a.transactionAt))}
          getRejectedOnly={getRejectedOnly}
          getAcceptedOnly={getAcceptedOnly}
        />
      </View>
      }
      {!isDataLoaded() &&
      <View style={styles.topWrapper}>
        <ActivityIndicator size="large" color="#0000ff"/>
      </View>}
    </View>
  );
};

export default ScanHistoryView;
