import { StyleSheet } from "react-native";
import globalStylesheet from '../../Utils/globalStylesheet';

const styles = StyleSheet.create({
  container: {
    ...globalStylesheet.container,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#FFF',
  },
  topWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  transactionListWrapper: {
    flex: 1
  },
});
export default styles;
