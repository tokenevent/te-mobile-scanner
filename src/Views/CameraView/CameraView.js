import React, { useRef, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import styles from './CameraView.style';
import globalStylesheet from '../../Utils/globalStylesheet';
import { NavigationEvents } from 'react-navigation';

const CameraView: () => React$Node = ({ navigation }) => {
  const tokensData = navigation.getParam('tokens', {});
  const [isFocused, setFocused] = useState(false);
  const camera = useRef(null);


  const handleBarCodeRead = React.useCallback(walletId => {
    setFocused(false);
    navigation.push('WaitingAccept', { walletId: walletId, tokens: tokensData });
  }, [navigation]);

  return (
    <View style={globalStylesheet.container}>
      <NavigationEvents
        onWillFocus={() => setFocused(true)}
        onWillBlur={() => setFocused(false)}
      />
      {isFocused &&
      <RNCamera
        ref={camera}
        style={styles.camera}
        onBarCodeRead={(data, rawData, type) => handleBarCodeRead(data.data)}
      />
      }
      <View>
        <View>
          <TouchableOpacity
            style={globalStylesheet.normalBlueButtonContainer}
            onPress={() => navigation.push('Code', { tokensData })}
          >
            <Text style={globalStylesheet.normalBlueButtonText}>WPROWADŹ KOD</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.cancelBtn}>
          <TouchableOpacity
            style={globalStylesheet.normalCancelButtonContainer}
            onPress={() => navigation.navigate('Main')}
          >
            <Text style={globalStylesheet.normalBlueButtonText}>ANULUJ</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default CameraView;


