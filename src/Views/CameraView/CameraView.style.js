
const styles = {
  camera: {
    flex: 1,
    width: '100%',
  },
  cancelBtn: {
    marginTop: 5
  }
};

export default styles;
