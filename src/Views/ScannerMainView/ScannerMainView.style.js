import { Dimensions, StyleSheet } from 'react-native';
import globalStylesheet from '../../Utils/globalStylesheet';

const WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    ...globalStylesheet.container,
    padding: 10,
    position: 'relative',
  },
  bottomWrapper: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#00000000',
    alignItems: 'center',
    marginTop: 1,
  },
  sendButtonWrapper: (isValid) => ({
    marginTop: 5,
    opacity: isValid ? 1 : 0.5,
  }),
  roundButtonWrapper: (disabled) => ({

    width: 77,
    height: 77,
    zIndex: 9999,
    backgroundColor: '#0E7AF5',
    borderRadius: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    opacity: disabled ? 0.5 : 1,

  }),
  plus: {
    color: 'white',
    fontSize: 34,
    fontFamily: 'TwCenMT-Condensed',
  },
  downButtonsContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  downButtonsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 160,
  },
});

export default styles;
