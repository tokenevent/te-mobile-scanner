import React, { useCallback, useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import styles from './ScannerMainView.style';
import { filterSelectedOptions } from '../../Utils/helpers';
import globalStylesheet from '../../Utils/globalStylesheet';
import TokenRow from '../../Components/TokenRow/TokenRow';
import UserService from '../../Services/UserService';
import { NavigationEvents } from 'react-navigation';
import EventService from '../../Services/EventService';
import logout from '../../Utils/logout';
import AsyncStorage from '@react-native-community/async-storage';
import repeat from './../../../assets/images/repeat.png';

const ScannerMainView: () => React$Node = ({ navigation, ...props }) => {
  const [tokens, setTokens] = useState([]);
  const [tokensToSelect, setTokensToSelect] = useState([]);
  const [isLoaded, setLoaded] = useState(false);


  useEffect(() => {
    getTokensToSelect();
  }, []);

  const addToken = useCallback(tokensToSelect => {
    const id = Math.floor(Date.now());
    const newTokens = Object.assign([], tokens);
    const filterResult = filterSelectedOptions(tokensToSelect, tokens);
    const newToken = {
      id: `token-${id}`,
      name: `token-${id}`,
      amount: '',
      type: filterResult[0].value || tokensToSelect[0].value,
      options: filterSelectedOptions(tokensToSelect, tokens),
    };
    newTokens.push(newToken);
    for (let i = 0; i < tokens.length; i++) {
      if (newTokens[i].id !== newToken.id) {
        newTokens[i].options = newTokens[i].options.filter(o => o.value !== newToken.type);
      }
    }
    setTokens([...newTokens]);
  }, [tokens, setTokens]);

  const removeToken = useCallback((id, index) => {
    if (id) {
      const tokenToRemove = tokens[index];
      const newTokens = [...tokens.filter(x => x.id !== id)];
      for (let i = 0; i < newTokens.length; i++) {
        const selectObj = tokensToSelect.filter(x => x.value === tokenToRemove.type)[0];
        newTokens[i].options.push(selectObj);
      }
      setTokens([...newTokens]);
    }
  }, [tokens, setTokens]);

  const onTypeChange = useCallback((type, index) => {
    let newTokens = Object.assign([], tokens);
    for (let i = 0; i < tokens.length; i++) {
      if (index === i) {
        newTokens[index].type = type;
      } else {
        newTokens[i].options = newTokens[i].options.filter(o => o.value !== type);
      }
    }
    setTokens([...newTokens]);
  }, [tokens, setTokens]);

  const onAmountChange = (amount, index) => {
    let newTokens = Object.assign([], tokens);
    const isValid = /^[0-9]+$/.test(amount) || amount === '' || !amount;
    if (isValid) {
      newTokens[index].amount = amount;
      setTokens([...newTokens]);
    }
  };


  const isFormValid = () => {
    return tokens.length > 0 && !tokens.some(t => !t.amount);
  };


  const parseTokens = selected => {
    return selected.map(t => {
      return {
        id: t.type,
        quantity: t.amount,
      };
    });
  };

  const getEventByIdSuccessHandler = React.useCallback(response => {
    const tokensParsed = response.data.buyableTokens.map(t => {
      return {
        label: t.name,
        value: t.id,
      };
    });
    setTokensToSelect([...tokensParsed]);
    setLoaded(true);
  });

  const getMeSuccessHandler = React.useCallback(async response => {
    if (response && response.data && response.data.user.eventId) {
      EventService.getEventById(response.data.user.eventId)
        .then(resp => getEventByIdSuccessHandler(resp))
        .catch(err => {
          ToastAndroid.show('Wystąpił błąd podczas pobierania danych wydarzenia...', ToastAndroid.SHORT);
          setLoaded(true);
        });
    } else {
      ToastAndroid.show('Wystąpił błąd podczas pobierania danych wydarzenia...', ToastAndroid.SHORT);
      setLoaded(true);
    }
  });

  const getTokensToSelect = () => {
    UserService.getMe()
      .then(response => getMeSuccessHandler(response))
      .catch(error => {
        logout(navigation);
        ToastAndroid.show('Wystąpił błąd podczas pobierania danych użytkownika...', ToastAndroid.SHORT);
        setLoaded(true);
      });

  };

  const setButtonsFromLastTransaction = () => {
    AsyncStorage.getItem('lastTransaction')
      .then(r => {
        if (r && r.length > 0) {
          const selectedObj = JSON.parse(r);
          setTokens(selectedObj);
        }
      })
      .catch(e => console.log('setButtonsFromLastTransaction error', e));
  };

  const redirectToScanTokens = React.useCallback((selectedTokens) => {
    AsyncStorage.setItem('lastTransaction', JSON.stringify(selectedTokens))
      .then(r => {
        setTokens([]);
        navigation.push('Camera', {
          tokens: parseTokens(selectedTokens),
        });
      })
      .catch(e => {
        console.log('redirectToScanTokens error', e);
        ToastAndroid.show('Zapisanie ostatniej transakcji nie powiodło się', ToastAndroid.SHORT);
      });
  }, []);


  return (

    <SafeAreaView style={globalStylesheet.container}>
      {isLoaded && <>
        <View style={styles.container}>
          <NavigationEvents
            onDidFocus={() => getTokensToSelect()}
          />
          <View>
            <Text style={globalStylesheet.header}>Ilość żetonów</Text>
          </View>
          <View style={{ marginBottom: 50 }}>
            <FlatList
              data={tokens}
              renderItem={({ item, index }) => (
                <TokenRow
                  token={item}
                  index={index}
                  onAmountChange={onAmountChange}
                  tokens={tokens}
                  onTypeChange={onTypeChange}
                  onRemove={removeToken}/>)}
              keyExtractor={token => token.id}
            />
          </View>
        </View>

        {tokensToSelect && tokensToSelect.length > 0 &&
        <View style={styles.downButtonsContainer}>
          <View style={styles.downButtonsWrapper}>
            <TouchableOpacity
              onPress={() => addToken(tokensToSelect)}
              disabled={tokens.length === tokensToSelect.length}
              style={styles.roundButtonWrapper(tokens.length === tokensToSelect.length)}>
              <Text style={styles.plus}>+</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setButtonsFromLastTransaction()}
              style={styles.roundButtonWrapper(tokens.length === tokensToSelect.length)}>
              <Image
                resizeMode="contain"
                source={repeat}
              />
            </TouchableOpacity>
          </View>
        </View>
        }

        <View style={styles.sendButtonWrapper(isFormValid())}>
          <TouchableOpacity disabled={tokens.length === 0 || !isFormValid()}
                            onPress={() => redirectToScanTokens(tokens)}
                            style={globalStylesheet.normalBlueButtonContainer}>
            <Text style={globalStylesheet.normalBlueButtonText}>SKANUJ</Text>
          </TouchableOpacity>
        </View>
      </>}
    </SafeAreaView>
  );
};

export default ScannerMainView;

