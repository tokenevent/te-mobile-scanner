import React, { useState } from 'react';
import { Image, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import styles from './WaitingForAcceptView.style';
import loader from './../../../assets/images/loader.gif';
import globalStylesheet from '../../Utils/globalStylesheet';
import transactionService from './../../Services/TransactionService';
import transactionStatus from '../../Utils/transactionStatus';
import errorCodes from '../../Utils/errorCodes';
import { NavigationEvents } from "react-navigation";

const INTERVAL = 2000;

let intervalHandler = null;

const WaitingForAcceptView: () => React$Node = ({ navigation }) => {

  const tokens = navigation.getParam('tokens', undefined);
  const walletId = navigation.getParam('walletId', undefined);
  const [transactionId, setTransactionId] = useState(undefined);

  const checkTransactionStatus = React.useCallback(async transactionId => {
    try {
      if (transactionId) {
        const response = await transactionService.checkTransactionStatusByTransactionId(transactionId);
        if (response && response.data) {
          if (response.data.transaction.status === transactionStatus.ACCEPTED) {
            clearInterval(intervalHandler);
            intervalHandler = null;
            navigation.navigate('SuccessScan');
          }
          if (response.data.transaction.status === transactionStatus.REJECTED) {
            clearInterval(intervalHandler);
            intervalHandler = null;
            navigation.navigate('ErrorScan', {
              errorTitle: errorCodes.TRANSACTION_REJECTED.title,
              errorDescription: errorCodes.TRANSACTION_REJECTED.description,
            });
          }
        }
      }
    } catch (e) {
      clearInterval(intervalHandler);
      intervalHandler = null;
      navigation.navigate('ErrorScan', {
        errorTitle: errorCodes.TRANSACTION_REJECTED.title,
        errorDescription: errorCodes.TRANSACTION_REJECTED.description,
      });
    }

  }, []);


  const fetchTransaction = React.useCallback(async () => {
    try {
      if (walletId && tokens) {
        const response = await transactionService.getTransactionByWalletId(walletId, tokens);
        if (response && response.data) {
          const transactionId = response.data.transaction.id;
          setTransactionId(transactionId);
          intervalHandler = setInterval(() => checkTransactionStatus(transactionId), INTERVAL);
          checkTransactionStatus(transactionId);
        }
      }
    } catch (e) {
      clearInterval(intervalHandler);
      intervalHandler = null;
      console.log('fetchTransaction error', e.response);
      if (e.response.status === errorCodes.NOT_ENOUGH_TOKENS.code) {
        navigation.navigate('ErrorScan', {
          errorTitle: errorCodes.NOT_ENOUGH_TOKENS.title,
          errorDescription: errorCodes.NOT_ENOUGH_TOKENS.description,
        });
      } else {
        navigation.navigate('ErrorScan', {
          errorTitle: errorCodes.TRANSACTION_REJECTED.title,
          errorDescription: errorCodes.TRANSACTION_REJECTED.description,
        });
      }
    }

  }, []);


  React.useEffect(
    () => {
      fetchTransaction();
    },
    [fetchTransaction],
  );

  const cancelScan = async idTransaction => {
    clearInterval(intervalHandler);
    transactionService.rejectTransaction(idTransaction)
      .then(response => {
        clearInterval(intervalHandler);
        intervalHandler = null;
        navigation.navigate('Sample');
      })
      .catch(err => {
        clearInterval(intervalHandler);
        intervalHandler = null;
        navigation.navigate('Sample');
        ToastAndroid.show('Wystąpił błąd podczas pobierania danych wydarzenia...', ToastAndroid.SHORT);
      });
  };

  return (
    <View style={globalStylesheet.container}>
      <NavigationEvents
        onWillBlur={() => {
          clearInterval(intervalHandler);
          intervalHandler = null;
        }}
      />
      <View style={styles.contentContainer}>
        <Text style={styles.header}>Oczekiwanie na akceptację</Text>
        <View style={styles.imageWrapper}>
          <Image
            source={loader}
          />
        </View>
      </View>
      <View>
        {transactionId && <>
          <TouchableOpacity
            style={globalStylesheet.normalCancelButtonContainer}
            onPress={() => cancelScan(transactionId)}
          >
            <Text style={globalStylesheet.normalBlueButtonText}>ANULUJ</Text>
          </TouchableOpacity>
        </>}
      </View>
    </View>
  );
};

export default WaitingForAcceptView;


