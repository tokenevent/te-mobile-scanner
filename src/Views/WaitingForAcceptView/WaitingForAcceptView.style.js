import globalStylesheet from '../../Utils/globalStylesheet';


const styles = {
  header: {
    ...globalStylesheet.header,
    fontSize: 42
  },
  contentContainer: { padding: 15, flex: 1 },
  imageWrapper: { flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }
};
export default styles;
