import React, { useEffect } from 'react';
import { ActivityIndicator, StatusBar, View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './AuthLoadingScreen.style';

export default function AuthLoadingScreen({ navigation }) {

  const _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('accessToken');
    navigation.navigate(userToken ? 'App' : 'Auth');
  };

  useEffect(() => {
    _bootstrapAsync();
  });


  return (
    <View style={styles.container}>
      <ActivityIndicator/>
      <StatusBar barStyle="default"/>
    </View>
  );
}
