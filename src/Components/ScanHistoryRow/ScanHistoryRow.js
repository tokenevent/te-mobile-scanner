import React, { useEffect } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import styles from './ScanHistoryRow.style';
import successImage from '../../../assets/images/success.png';
import failedImage from '../../../assets/images/close.png';
import { toPlnString } from '../../Utils/helpers';
import TransactionService from '../../Services/TransactionService';
import TransactionConsentBox from '../TransactionConsentBox/TransactionConsentBox';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';

const ScanHistoryRow = ({ transaction, buttonsHighlight }) => {

  const totalPrice = transaction.tokens.totalValue;
  const totalString = toPlnString(totalPrice);
  const transactionDate = `${new Date(transaction.transactionAt).toLocaleDateString()}, ${new Date(transaction.transactionAt).toLocaleTimeString()}`;
  const [isDetailsExpanded, setExpand] = React.useState(false);
  const [isLoading, setLoading] = React.useState(false);
  const [transactionDetails, setTransactionDetails] = React.useState({});

  useEffect(() => {
    setExpand(false);
  }, [buttonsHighlight]);

  const getDetails = id => {
    if (!(transactionDetails && transactionDetails.userName)) {
      setLoading(true);
      TransactionService.getTransactionDetails(id)
        .then(response => {
          setLoading(false);
          setTransactionDetails(response.data);
        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        });
    }
    setExpand(!isDetailsExpanded);
  };


  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.transaction}
        onPress={() => {
          getDetails(transaction.id);
        }}>
        <View style={styles.container_token}>
          <Text>Ilość</Text>
          <Text style={styles.tokensCount}>{transaction.tokens.count}</Text>
        </View>
        <View style={styles.container_text}>
          <Text style={styles.title}>
            {transaction.eventName}
          </Text>
          <Text style={styles.description}>
            Wartość zakupów: {totalString}zł{'\n'}
            Data transakcji {transactionDate}{'\n'}
            Zeskanowano: <Text style={{ fontWeight: 'bold' }}>{transaction.userName}</Text>
          </Text>
        </View>
        {transaction.status === 'ACCEPTED'
        &&
        <View>
          <Image
            style={styles.successImg}
            source={successImage}
            resizeMode="contain"
          />
          <Text style={styles.successDesc}>
            PRZYJĘTA
          </Text>
        </View>
        }
        {transaction.status === 'REJECTED'
        &&
        <View>
          <Image
            style={styles.failedImg}
            source={failedImage}
            resizeMode="contain"
          />
          <Text style={styles.failDesc}>
            ODRZUCONA
          </Text>
        </View>
        }
      </TouchableOpacity>
      {isDetailsExpanded && !isLoading && transactionDetails &&
      (transactionDetails.tokens || []).map((t, index) => {
        return <TransactionConsentBox
          key={index}
          amount={t.amount}
          unitPrice={t.unitPrice}
          tokenName={t.buyableTokenName}
        />;
      })
      }
      {isLoading && <LoadingIndicator withMargin={false} loadingText="Wczytywanie szczegółów..."/>}
    </View>
  );
};


export default ScanHistoryRow;
