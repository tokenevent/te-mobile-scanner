import React from 'react'
import { ActivityIndicator, Text, View } from 'react-native'
import styles from './LoadingIndicator.style';

const LoadingIndicator = ({ loadingText = 'Trwa ładowanie...', withMargin = true }) => {

  return (
    <View style={withMargin ? styles.container : styles.containerWithoutMargin}>
      <ActivityIndicator size="large" color="#0E7AF5"/>
      <Text style={styles.text}>{loadingText}</Text>
    </View>
  )
};
export default LoadingIndicator


