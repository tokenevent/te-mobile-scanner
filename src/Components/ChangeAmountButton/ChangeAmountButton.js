import { Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import styles from './ChangeAmountButton.style';
const ChangeAmountButton = ({ color, onChange, changeAmount, currentAmount }) => {

  const change = changeAm => {
    let result = Number(currentAmount) + Number(changeAm);
    if (result < 0) {
      result = 0;
    }
    onChange(String(result));
  };

  return <View>
    <TouchableOpacity
      onPress={() => change(changeAmount)}
      style={styles.opacity(color)}
    >
      <Text style={styles.changeAmountText}>{changeAmount}</Text>
    </TouchableOpacity>
  </View>;

};
export default ChangeAmountButton;
