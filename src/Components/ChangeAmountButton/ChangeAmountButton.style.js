import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  opacity: color => ({
    width: '100%',
    backgroundColor: color,
    margin: 2,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  }),
  changeAmountText: { color: 'white' },
});

export default styles;
