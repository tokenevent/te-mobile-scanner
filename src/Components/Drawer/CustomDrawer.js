import React, { useState } from 'react';
import { Image, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { DrawerNavigatorItems, TouchableItem } from 'react-navigation-drawer';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './CustomDrawer.style';
import deviceStorage from '../../Utils/deviceStorage';


const CustomDrawer = ({ navigation, descriptors, ...props }) => {
  const [userName, setUserName] = useState('');
  const [eventName, setEventName] = useState('');
  const navLink = React.useCallback((onClickFunction, text, icon) => {
    return (
      <TouchableOpacity style={{ height: 50 }} onPress={onClickFunction}>
        <View style={{ marginLeft: 18 }}>
          <View style={styles.menuPositionContainer}>
            <Image style={styles.menuPosition}
                   source={icon}
                   resizeMode='stretch'/>
            <Text style={styles.link}>{text}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }, []);

  const signOut = async () => {
    await AsyncStorage.clear();
    navigation.navigate('Auth');
  };

  React.useEffect(() => {
    getScannerData();
  },[]);

  const getScannerData = React.useCallback(async () => {
    const userName = await deviceStorage.getItem('userName');
    const eventName = await deviceStorage.getItem('eventName');
    setUserName(userName);
    setEventName(eventName)
  }, [deviceStorage, setUserName, setEventName]);

  return (
    <View style={styles.container}>
      <ScrollView style={styles.scroller}>
        <LinearGradient style={styles.gradient} colors={['#024492', '#148EC1']} useAngle={true} angle={45}
                        angleCenter={{ x: 0.5, y: 0.5 }}>
          <View style={styles.topLinks}>
            <View style={styles.profile}>
              <View style={styles.imgView}>
                <Image style={styles.img} source={require('../../../assets/images/placeholder.jpg')}/>
              </View>
              <View style={styles.profileText}>
                <Text style={styles.name}>{userName}</Text>
                <Text style={styles.eventName}>{eventName}</Text>
              </View>
            </View>
          </View>
          <View style={styles.bottomLinks}>
            <DrawerNavigatorItems
              {...props}
              labelStyle={styles.drawerMenuLink}
            />
            {navLink(signOut, 'WYLOGUJ', require('./../../../assets/images/logoutIco.png'))}
          </View>
        </LinearGradient>
      </ScrollView>
    </View>
  );
};

export default CustomDrawer;
