import React from 'react'
import { Text, View } from 'react-native'
import globalStylesheet from "../../Utils/globalStylesheet";
import styles from './TransactionConsentBox.style'

const TransactionConsentBox = ({ tokenName, unitPrice, amount }) => {

  return (
    <View style={styles.mainWrapper}>
      <View style={styles.columnWrapper}>
        <View style={styles.rowWrapper}>
          <Text style={globalStylesheet.tokenName}>{tokenName}</Text>
          <Text style={globalStylesheet.slash}>/</Text>
          <Text style={globalStylesheet.unitPrice}>{unitPrice / 100} zł</Text>
        </View>
        <View style={styles.rowWrapper}>
          <Text style={globalStylesheet.ordered}>Wartość: </Text>
          <Text style={globalStylesheet.orderedAmount}>{unitPrice / 100 * amount}</Text>
          <Text style={globalStylesheet.szt}>zł</Text>
        </View>
      </View>
      <View style={styles.amountWrapper}>
        <Text style={globalStylesheet.availableTokens}>{amount}</Text>
        <Text style={globalStylesheet.availableQty}> szt.</Text>
      </View>
    </View>
  )
};

export default TransactionConsentBox;
