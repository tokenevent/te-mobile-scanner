import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  mainWrapper: { display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'},
  columnWrapper: { display: 'flex', flexDirection: 'column' },
  rowWrapper: { display: 'flex', flexDirection: 'row', alignItems: 'center' },
  amountWrapper: {display: 'flex', flexDirection: 'row', alignItems: 'baseline'}
});
export default styles
