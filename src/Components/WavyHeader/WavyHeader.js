import React from 'react';
import { SvgXml, Svg } from 'react-native-svg';


const xml = `
<svg
  version="1.1"
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  viewBox="0 0 375 50"
  preserveAspectRatio="none"
  width="100%"
  height="50px"
  >
      <style>
      .blur { 
    filter: url('#dropshadow');
    }
    </style>
       <filter id="dropshadow" x="-2" y="-2" width="200" height="200">
      <feGaussianBlur  stdDeviation="1"/>
    </filter>
  <defs>
    <path   
        class="blur"
      d="M375.34 0L0 0L0 46.1C-0.01 45.93 0.99 45.93 3 46.1C89.52 53.19 114.35 49.65 133.5 50C152.65 50.35 222.86 41.84 272.5 40.43C305.6 39.48 339.88 41.37 375.34 46.1L375.34 0Z"
      id="f533tPXZKt"/>
  </defs>
  <g>
    <g>
      <g>
        <use xlink:href="#f533tPXZKt" opacity="1" fill="#0e7af5" fill-opacity="1"/>
        <g>
          <use xlink:href="#f533tPXZKt" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="0"
               stroke-opacity="1"/>
        </g>
      </g>
    </g>
  </g>
</svg>
`;

export default () => <SvgXml xml={xml} width="100%"/>;
