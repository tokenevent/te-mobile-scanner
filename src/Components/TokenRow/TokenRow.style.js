import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  minusWrapper: {
    width: 55,
    height: 55,
    backgroundColor: '#F50E0E',
    borderRadius: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  minus: {
    color: 'white',
    fontSize: 40,
    fontFamily: 'TwCenMT-Condensed',
    marginTop: -8
  },
  roundWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  pickerItem: {
    fontSize: 19,
    fontFamily: 'TwCenMT-Condensed'
  },
  picker: {
    height: 58,
    width: "100%",
    borderBottomColor: "grey",
    borderBottomWidth: 2,
    borderColor: "#707070"
  },
  pickerWrapper: {
    borderBottomColor: "black",
    borderBottomWidth: 2,
    width: "38%",
    marginLeft: 5
  },
  input: (disabled) => ({
    fontSize: 28,
    height: 60,
    borderBottomColor: disabled ? "red" : "#707070",
    borderBottomWidth: 2,
    width: "43%",
    color: 'black',
    fontFamily: 'TwCenMT'
  }),
  wrapper: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    marginTop: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonWrapper: { display: 'flex', flexDirection: 'row', justifyContent: 'center' },
  singleButton: {
    width: '14.5%',
    margin: 3
  }
});

export default styles;
