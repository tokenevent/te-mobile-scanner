import { Picker, Text, TextInput, TouchableOpacity, View } from 'react-native';
import React from 'react';
import styles from './TokenRow.style';
import ChangeAmountButton from '../ChangeAmountButton/ChangeAmountButton';

const TokenRow = ({ token, index, onAmountChange, onTypeChange, onRemove }) => {

  const validate = React.useCallback((amount, index) => {
    if (/^[1-9]\d*$/.test(amount) || amount === '') {
      onAmountChange(amount, index);
    }
  }, [onAmountChange]);

  return <View>
    <View style={styles.wrapper}>
      <TextInput
        style={styles.input(!token.amount)}
        onChangeText={amount => validate(amount, index)}
        placeholder="Ilość"
        value={token.amount}
        keyboardType="numeric"
      />
      <View style={styles.pickerWrapper}>
        <Picker
          selectedValue={token.type}
          style={styles.picker}
          onValueChange={itemValue => onTypeChange(itemValue, index)}>
          {token.options.map((o, key) => <Picker.Item
            key={key}
            color="black"
            itemStyle={styles.pickerItem}
            label={o.label}
            value={o.value}/>)
          }
        </Picker>
      </View>
      <View style={styles.roundButtonWrapper}>
        <TouchableOpacity
          onPress={() => onRemove(token.id, index)}
          style={styles.minusWrapper}>
          <Text style={styles.minus}>-</Text>
        </TouchableOpacity>
      </View>


    </View>
    <View style={styles.buttonWrapper}>
      <View style={styles.singleButton}>
        <ChangeAmountButton changeAmount='-1' color='#F50E0E' currentAmount={token.amount}
                            onChange={amount => validate(amount, index)}/>
      </View>
      <View style={styles.singleButton}>
        <ChangeAmountButton changeAmount='-2' color='#F50E0E' currentAmount={token.amount}
                            onChange={amount => validate(amount, index)}/>
      </View>
      <View style={styles.singleButton}>
        <ChangeAmountButton changeAmount='-5' color='#F50E0E' currentAmount={token.amount}
                            onChange={amount => validate(amount, index)}/>
      </View>
      <View style={styles.singleButton}>
        <ChangeAmountButton changeAmount='+5' color='#0E7AF5' currentAmount={token.amount}
                            onChange={amount => validate(amount, index)}/>
      </View>
      <View style={styles.singleButton}>
        <ChangeAmountButton changeAmount='+2' color='#0E7AF5' currentAmount={token.amount}
                            onChange={amount => validate(amount, index)}/>
      </View>
      <View style={styles.singleButton}>
        <ChangeAmountButton changeAmount='+1' color='#0E7AF5' currentAmount={token.amount}
                            onChange={amount => validate(amount, index)}/>
      </View>
    </View>
  </View>;
};

export default TokenRow;
