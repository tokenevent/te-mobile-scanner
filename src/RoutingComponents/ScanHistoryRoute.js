import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import ScanHistoryView from '../Views/ScanHistoryView/ScanHistoryView';
import Header from '../Components/Header/Header';

const ScanHistoryRoute = createStackNavigator({
  HistoryScan: {
    screen: ScanHistoryView,
    navigationOptions: {
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default ScanHistoryRoute;
