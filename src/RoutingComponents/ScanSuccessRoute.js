import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import ScanSuccessView from '../Views/ScanSuccessView/ScanSuccessView';
import Header from '../Components/Header/Header';


const ScanSuccessRoute = createStackNavigator({
  SuccessScan: {
    screen: ScanSuccessView,
    navigationOptions: {
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default ScanSuccessRoute;
