import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import CameraView from '../Views/CameraView/CameraView';

const CameraRoute = createStackNavigator({
  Camera: {
    screen: CameraView,
    navigationOptions: {
      header: null,
    },
  },
});
export default CameraRoute;
