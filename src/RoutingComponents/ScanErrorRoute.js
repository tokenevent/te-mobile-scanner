import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import ScanErrorView from '../Views/ScanErrorView/ScanErrorView';
import Header from '../Components/Header/Header';

const ScanErrorRoute = createStackNavigator({
  ErrorScan: {
    screen: ScanErrorView,
    navigationOptions: {
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default ScanErrorRoute;
