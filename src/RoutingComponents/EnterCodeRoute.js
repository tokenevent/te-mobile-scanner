import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import EnterCodeView from '../Views/EnterCodeView/EnterCodeView';
import Header from '../Components/Header/Header';

const EnterCodeRoute = createStackNavigator({
  Code: {
    screen: EnterCodeView,
    navigationOptions: {
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default EnterCodeRoute;
