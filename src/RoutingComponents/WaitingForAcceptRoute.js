import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import WaitingForAcceptView from '../Views/WaitingForAcceptView/WaitingForAcceptView';

const WaitingForAcceptRoute = createStackNavigator({
  WaitingAccept: {
    screen: WaitingForAcceptView,
    navigationOptions: {
      header: null,
    },
  },
});
export default WaitingForAcceptRoute;
