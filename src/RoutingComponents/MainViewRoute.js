import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import ScannerMainView from '../Views/ScannerMainView/ScannerMainView';
import CustomDrawer from '../../App';
import Header from '../Components/Header/Header';


const MainViewRoute = createStackNavigator({
  Sample: {
    screen: ScannerMainView,
    navigationOptions: {
      contentComponent: props => <CustomDrawer {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default MainViewRoute;
