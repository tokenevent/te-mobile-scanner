import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import MainViewRoute from './src/RoutingComponents/MainViewRoute';
import AuthLoadingScreen from './src/Views/AuthLoadingScreen/AuthLoadingScreen';
import LoginView from './src/Views/LoginView/LoginView';
import CameraRoute from './src/RoutingComponents/CameraRoute';
import WaitingForAcceptRoute from './src/RoutingComponents/WaitingForAcceptRoute';
import EnterCodeRoute from './src/RoutingComponents/EnterCodeRoute';
import ScanSuccessRoute from './src/RoutingComponents/ScanSuccessRoute';
import ScanErrorRoute from './src/RoutingComponents/ScanErrorRoute';
import CustomDrawer from './src/Components/Drawer/CustomDrawer';
import ScanHistoryRoute from './src/RoutingComponents/ScanHistoryRoute';
import { Image, StyleSheet } from 'react-native';

const Hidden = () => null;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scroller: {
    flex: 1,
    backgroundColor: '#024492',
  },
  city: {
    fontSize: 10,
    paddingBottom: 5,
    paddingTop: 5,
    color: 'rgba(255,255,255,0.7)',
    textAlign: 'center',
  },
  profile: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 25,
    borderBottomColor: '#777777',
  },
  profileText: {
    flex: 3,
    marginTop: 10,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  name: {
    fontSize: 20,
    paddingBottom: 5,
    color: 'white',
    textAlign: 'left',
  },
  imgView: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
  },
  img: {
    height: 120,
    width: 120,
    borderRadius: 60,
    borderWidth: 10,
    borderColor: '#3A99C5',
  },
  topLinks: {
    height: 260,
    marginBottom: -50,
  },
  bottomLinks: {
    flex: 1,
    paddingTop: 20,
  },
  link: {
    flex: 1,
    fontSize: 20,
    padding: 6,
    paddingLeft: 14,
    margin: 5,
    fontFamily: 'TwCenMT',
    textAlign: 'left',
    color: 'white',
  },
  menuPosition: {
    height: 20,
    width: 20,
    marginRight: 3,
  },
  menuPositionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pin: {
    height: 20,
    width: 13,
    marginRight: 5,
    opacity: 0.5,
  },
});



const AppStack = createDrawerNavigator({
    Main: {
      screen: MainViewRoute,
      navigationOptions: {
        drawerIcon: <Image style={styles.menuPosition} source={require('./assets/images/tokenIco.png')} resizeMode='stretch'/>,
        title: 'Wybór żetonów'
      }
    },
    Camera: {
      screen: CameraRoute,
      navigationOptions: {
        drawerLockMode: 'locked-closed',
        drawerLabel: <Hidden/>,
      },
    },
    EnterCode: {
      screen: EnterCodeRoute,
      navigationOptions: {
        drawerLabel: <Hidden/>,
      },
    },
    WaitingForAccept: {
      screen: WaitingForAcceptRoute,
      navigationOptions: {
        drawerLockMode: 'locked-closed',
        drawerLabel: <Hidden/>,
      },
    },
    ScanSuccess: {
      screen: ScanSuccessRoute,
      navigationOptions: {
        drawerLockMode: 'locked-closed',
        drawerLabel: <Hidden/>,
      },
    },
    ScanError: {
      screen: ScanErrorRoute,
      navigationOptions: {
        drawerLabel: <Hidden/>,
      },
    },
    ScanHistory: {
      screen: ScanHistoryRoute,
      navigationOptions: {
        title: 'Historia',
        drawerIcon: <Image style={styles.menuPosition} source={require('./assets/images/calendarIco.png')} resizeMode='stretch'/>
      }
    },

  },
  {
    contentComponent: CustomDrawer,
    drawerWidth: 300,
  },
);

const AuthStack = createSwitchNavigator({ LoginView });

const AppRoute = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: {
      screen: AppStack,

    },
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);


export default createAppContainer(AppRoute);

